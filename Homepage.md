# Home page
## _MAJOR_ “Get started” guide doesn’t help solve main problem
After this convenient step-by-step chat onboarding experience, “Get started” guide feels like it shoots at me from all turrets at once:
- In “Learn the basics”, I should play videos, flip through presentations, read in-depth books and articles about methodologies.
- In “First steps” I should learn Weekdone interface  by doing things that are not very meaningfull at this moment. Why do I need to comment something about some example objective in a first place?

On the other hand, the main block suggest you to: 
- Write down your, your team, and company _weekly_ plans (Is this the right approach to start from short-term goals?).
- Write down your (later, please!), your team (I didn’t set it up yet!), and company (at last!) OKRs.
You see, I’ve just registered as lead of my own small consultancy. I’ve read a bit about OKR methodology on marketing page before registering, and willing to start setting my company goals. My _personal_ OKRs can wait. Please guide me only through essential core features that help me reach my current goal: add company objectives and break them into key results.

__Local solution:__ Present company manager with tools that will help solving his main task, and introduce other  functionality later and in small portions. Rename ‘First steps’ into ‘Achievements’ as they essentially are, and introduce them later too.

## _MEDIUM_ “Create new objective” wizard has wrong defaults
I’m just registered as company manager, and I’m looking at how your product will solve my problem: to set up company OKRs. Why default assignee is me? Also, it’s middle of march, your software can definitely suggest that I’m creating a plan for Q2.

__Local solution:__ set defaults based on user role and current date.

## _CRITICAL_ “Create new objective” wizard loses my data
After entering an Objective and couple of KRs, before going to “Linking” step, closing wizard will not save entered O and KRs.

__Local solution:__ save entered O and KRs at the end of each step, not at the end of wizard.

## _MEDIUM_ Hard to get to /gettingstarted page
After more than an hour of setting up Weekdone, I just accidentally clicked “Here's how Weekdone items relate:” header, and landed on separte “Getting started” page. I’ve seen some of its parts in “Learn the basics” popover. Why there’s such division?

__Local solution:__ Provide getting started experience in single place that is easy to reach to.

## _MINOR, but hey_ Company, team and person avatars
I’m very pleased that you’ve picked my company logo from favicon of my email’s domain. Please don’t round it though, it cuts out all the beauty. Also, in 2019 there shoud definitely be the way to make consistent and well-centered avatars from initials. There also couple of places where it’s only one letter on avatar.

__Local solution:__ Don’t round company avatar if it comes from favicon. Unify and reslice avatars and other rounded interface elements.

## Global solution
Seems like there should be couple of different ‘getting started’ processes, based on company hierarchy and current user’s role: one for company manager, and at least one other for regular employee. They should help solve each role’s main task.

This kind of separation shoud also be reflected in different “First Steps” experiences. Instead of two current items in “Get started” popup, I’d create different Objectives based on user’s role _inside actual interface_ that would replace current “Example” Objectives. They would consist of KRs that reflect each role’s main task, e.g., for manager:
- O: Setup objectives for next quarter
	- KR: Setup 3 objectives for your company
	- KR: Setup at least 9 KRs for your company, minimum three for each objective
	- KR: Setup an Objective for you
	- KR: Setup 3 KRS for that objective
- O: Setup teams, departaments and people
	- KR: All employees are invited and registered in Weekdone (set amount of people as goal)
	- KR: All teams created and people are in their teams (set amount of teams as goal)
	- KR: All teams are grouped into departaments (optional, set amount of departaments as goal)
- O: Plan the first week in the upcoming quarter
	- KR: Create weekly plans for each department (set amount of departments as goal)

## Whys
- Why multiple scroll tracks are active when “Learn the basics” popover is open? Why it’s ‘close’ button scrolls too?
