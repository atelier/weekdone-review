# Onboarding
## _MAJOR_ Lack of hierarchy, multi-direction flow
I was presented with screen divided into two visually equal sections — chat screen and videos slider. It confused me and forced to decide what to do first: read chat or play video. I decided to read chat messages, top to bottom. At the end it wanted me to input my level of familiarity with OKR, but two messages earlier recommended to play video. Should I play it first? What if I answer, say, “Researched but not used”, and video on the right will be gone? Should I watch only one video or there are four more (I see dots)? This is unnecessary decision points for user.

__Local solution:__ change whole process to unidirectional, with less user decisions.

## _MINOR_ Unfinished bot personalization
Chatbot tried to mimic real person (using signature and Jüri’s photo as chat avatar), but forgot to introduce himself properly. I needed to decrypt signature to find out the name.

__Local solution:__ add name.
> “I’m Jüri Kaljundi, CEO of Weekdone…”.

## _MINOR_ Pseudo-interactive and visually different examples
In the slider on the right, there’s some O→KR hierarchy examples. One is just in different visual style, the others confused me with their pseudo-interactivity (see video). They feels out of context because I reached them before answering the first question, by just flipping the slider.

__Local solution:__ don’t allow user to flip slider manually, go to specific slide programmatically when chat conversation reaches key point.

## _MINOR_ Body copy mistake
Chat message says “…have a look at this short video on the _left_ …”, but it’s on the right actually.

__Local solution:__ “left” → “right”

## _MINOR_ Slider says “You already missed something”
There’s active “Back” control arrow on the left of slider, despite it’s on first slide, based on dots indicator below.

__Local solution:__ remove “Back” control on first slide.

## Global solution
Change whole process to unidirectional, linear top-to-bottom flow without branching. Prevent users from interacting with slider. Moreover, I would inline video block into chat message flow right after ivite to look at it; and inline that pseudo-interactive resulting O→KR hierarchy block (based on user input) instead of its current textual representation (O:…, KR:…, Plan:…). Then, I’d remove the slider and leave just terminal controls (’skip’ and ‘example quaterly objectives’) on the right. Also, polish messages copy and add more personalization.

## Whys
- Why answers differs in rounding?
- Why auto-close onboarding chat? Maybe, for regular users it’s okay, but I wanted to make some screenshots, and 10 seconds was not enough.
